import java.util.ArrayList;


public class Mapa {
	int columnas, filas;
	Casilla[][] quadricula;
	
	public Mapa(int cols, int fils){
		filas=fils;
		columnas=cols;

		quadricula=new Casilla[columnas][filas];

		for(int i = 0 ; i < columnas ; i ++){
			for(int j = 0 ; j < filas ; j++){
				quadricula[i][j]=new Casilla(i,j);
				if(i > 0 && i < columnas -1 && 
					j > 0 && j < filas -1  ||
					i==6 || j==6 ||
					i==5 || j==5 ||
					i==4 || j==4 ||
					i==3 || j==3 ||
					
					i==7 || j==7 ||
					i==8 || j==8 ||
					i==9 || j==9 ){
					quadricula[i][j].tipo="box";
					if(i%2==1 && j%2==1){
						quadricula[i][j].tipo="column";
					}
					if(i==j && i%2==1 ){
						quadricula[i][j].tipo="column";
					}
					
				} 
				
				
				
			}
			//System.out.println("");
		}
		for(int i = 0; i < columnas ; i ++){
			for(int j = 0 ; j < filas ; j++){
				if(quadricula[i][j].tipo.equalsIgnoreCase("box")){
					System.out.print("|O|");
				}else if(quadricula[i][j].tipo.equalsIgnoreCase("column")){
					System.out.print("|*|");
				}else{
					System.out.print("| |");
				}
			}
			System.out.println("");

		}
	}
}
