import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.StringTokenizer;


public class Server {
	
		Hashtable<Socket,Jugador> clientes = new Hashtable<Socket,Jugador>();
		boolean started= false;
		ServerSocket servidorS;
		Socket cliente;
		String preg,resp;
		Mapa mapa;
		
		/**
		 * crea una nueva conexion con el servidor y un nuevo canal para cada socket
		 */
		public void conexion(){
			int port = 6002;
			mapa = new Mapa(13,13);
			try{
				servidorS = new ServerSocket(port);
				while(true){
					cliente = servidorS.accept();
					System.out.println("cliente conectado " + cliente);
					//PrintWriter salida = new PrintWriter(cliente.getOutputStream());
					DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());
					clientes.put(cliente,new Jugador(salida));
					new Canal(this,cliente);
				}
			}catch(Exception E){

			}
		}
		
		
//		/**
//		 * envia una nueva pregunta a todos los conectados
//		 */
//		public void hacerPregunta(){
//			int chosenIndex = 0 + (int)(Math.random()*Quiz.preguntas.length-1);
//			preg=Quiz.preguntas[chosenIndex];
//			resp=Quiz.respuestas[chosenIndex];
//			hacerBroadcast("chat@TROLLMASTER:"+preg);
//		}
//		
		
		/**
		 * Devuelve los datos de un jugador conectado
		 * @param user socket relacionado con el usuario
		 * @return datos del usuario
		 */
		public Jugador UserDeSocket(Socket user){
			return clientes.get(user);
		}
		
		
		/**
		 * actualiza los jugadores y sus puntos. se ejecuta cuando alguien entra o sale.
		 * 
		 */
		public void actualizarJugadores(){
			String noms="";
			synchronized(clientes){
				Enumeration<Jugador> listado = clientes.elements();
				while(listado.hasMoreElements()){
					Jugador loki = listado.nextElement();
					noms +=loki.nombre+":"+loki.puntos+";";
				}
				decirATodos("usuarios@"+noms);
			}
			//publica la primera pregunta
//			if(clientes.size()>2 && !started){
//				hacerPregunta();
//				started=true;
//			}
			if(clientes.size()<1){
				started=false;
			}
		}
		/**
		 * manda texto a todos los usuarios conectados
		 * @param mensaje puede ser texto para contenido o la lista de usuarios
		 */
		public void decirATodos(String mensaje){
			synchronized(clientes){
				Enumeration<Socket> listado = clientes.keys();
				while ( listado.hasMoreElements() ){
					Socket cliente = (Socket)listado.nextElement();
					Jugador salida = clientes.get( cliente );
					try {
						salida.escritor.writeUTF(mensaje);
						salida.escritor.flush();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		/**
		 * manda orden de cerrar a un cliente concreto
		 * @param cliente socket de cliente que queremos cerrar
		 * @param mensaje orden de cerrar /bye
		 */
		public void mensajeCerrar(Socket clienteSocket, String mensaje){
			synchronized(clientes){
				Enumeration<Socket> listado = clientes.keys();
				while ( listado.hasMoreElements() ){
					Socket posiblecliente = (Socket)listado.nextElement();
					if(posiblecliente.equals(clienteSocket)){
						Jugador salida = clientes.get( posiblecliente );
						try{
							salida.escritor.writeUTF(mensaje);
							salida.escritor.flush();
						}catch(IOException e){
							//TODO
						}
					}
				}
			}
		}
		/**
		 * borra el cliente del array de clientes y cierra la conexion
		 * @param socket socket del cliente
		 */
		public void cerrarCliente(Socket socket){
			try{
				clientes.remove(socket);
				socket.close();
			}catch(IOException e){
				//TODO
			}
		}
	


//	/**
//	 * por casda cliente se crea un canal que gestiona la informacion entrante y saliente
//	 * @author Mikel
//	 *
//	 */
//	class Canal extends Thread{
//		QuizServer server;
//		Socket socketCli;
//
//		public Canal(QuizServer servidor, Socket socket){
//			this.server = servidor;
//			this.socketCli = socket;
//			this.start();
//		}
//		
//		public void run(){
//			try{
//				Scanner entrante = new Scanner(socketCli.getInputStream());
//				String bruto;
//				while(true){
//					if(entrante.hasNextLine()){
//						bruto = entrante.nextLine();
//						System.out.println(bruto);
//						StringTokenizer partes = new StringTokenizer(bruto,":");
//						int flag = 0;
//						if(partes.hasMoreElements()){
//							String usuario = partes.nextToken();
//				    		String mensaje = partes.nextToken();
//				    		//esta parte controla el contenido dle mensaje y actua en cosequiencia
//				    		//pueden ser ordenes del servidor, mensajes, preguntas o respuestas
//				    		if(usuario.equalsIgnoreCase("username")){
//				    			server.UserDeSocket(socketCli).nombre=mensaje;
//				    			server.actualizarJugadores();
//				    		}
//				    		else if(mensaje.equalsIgnoreCase(DataServer.closing) && flag == 0){
//								server.mensajeCerrar(socketCli, "chat@"+bruto);
//								server.cerrarCliente(socketCli);
//								entrante.close();
//								socketCli.close();
//								server.actualizarJugadores();
//								break;
//				    		}else if(mensaje.equalsIgnoreCase(server.resp)){
//								server.decirATodos("chat@"+bruto);
//								server.decirATodos("chat@TROLLMASTER:"+server.UserDeSocket(socketCli).nombre+" acerto y gano 10 puntos!");
//								server.UserDeSocket(socketCli).puntos+=10;
//				    			server.actualizarJugadores();
//				    			server.hacerPregunta();
//				    		}else if(mensaje.equalsIgnoreCase(DataServer.repeat)){
//				    			if(!server.started){
//				    				server.decirATodos("chat@"+bruto);
//					    			server.decirATodos("chat@TROLLMASTER:"+"se formulara una pregunta cuando haya mas de 2 usuarios");
//				    			}else{
//				    				server.decirATodos("chat@"+bruto);
//				    				server.decirATodos("chat@TROLLMASTER:"+server.preg);
//				    			}
//				    		}else if(mensaje.equalsIgnoreCase(DataServer.help)){
//								server.decirATodos("chat@"+bruto);
//				    			server.decirATodos("chat@TROLLMASTER:"+
//				    					"Trata de responder la pregunta antes que los otros usuarios. "+
//				    					"Escribe '/bye' para salir. "+
//				    					"Escribe '/rpt'para saber la pregunta. "
//				    					);
//							}else{
//								server.decirATodos("chat@"+bruto);
//							}
//							flag = 1;
//						}
//					}
//				}
//			}catch(IOException E){
//				//TODO
//			}finally{
//				server.cerrarCliente(socketCli);
//			}
//		}
//
	


	}
