import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;




public class Canal extends Thread{
	Server server;
	Socket socketCli;

	public Canal(Server servidor, Socket socket){
		this.server = servidor;
		this.socketCli = socket;
		this.start();
	}
	
	public void run(){
		try{
			Scanner entrante = new Scanner(socketCli.getInputStream());
			String bruto;
			while(true){
				if(entrante.hasNextLine()){
					bruto = entrante.nextLine();
					System.out.println(bruto);
					StringTokenizer partes = new StringTokenizer(bruto,":");
					int flag = 0;
					if(partes.hasMoreElements()){
						String usuario = partes.nextToken();
			    		String mensaje = partes.nextToken();
			    		//esta parte controla el contenido dle mensaje y actua en cosequiencia
			    		//pueden ser ordenes del servidor, mensajes, preguntas o respuestas
			    		if(usuario.equalsIgnoreCase("username")){//En el cliente, al ingresar el usuario su nombre, se enviara al servidro el String 'username@elnombreusuario'
			    			server.UserDeSocket(socketCli).nombre=mensaje;
			    			server.actualizarJugadores();
			    		}if(usuario.equalsIgnoreCase("skin")){//En el cliente, al ingresar el usuario su skin, se enviara al servidro el String 'skin@numerodeskin'
			    			server.UserDeSocket(socketCli).skin= Integer.parseInt(mensaje);
			    		}
			    		else if(mensaje.equalsIgnoreCase("close") && flag == 0){
							server.mensajeCerrar(socketCli, "chat@"+bruto);
							server.cerrarCliente(socketCli);
							entrante.close();
							socketCli.close();
							server.actualizarJugadores();
							break;
			    		}else if(mensaje.equalsIgnoreCase("up")){
			    			server.UserDeSocket(socketCli).posY+=1;
			    		}else if(mensaje.equalsIgnoreCase("down")){
			    			server.UserDeSocket(socketCli).posY-=1;
			    		}else if(mensaje.equalsIgnoreCase("left")){
			    			server.UserDeSocket(socketCli).posX-=1;
						}else if(mensaje.equalsIgnoreCase("right")){
			    			server.UserDeSocket(socketCli).posX+=1;
						}else{
							server.decirATodos("chat@"+bruto);
						}
						flag = 1;
					}
				}
			}
		}catch(IOException E){
			//TODO
		}finally{
			server.cerrarCliente(socketCli);
		}
	}

}
