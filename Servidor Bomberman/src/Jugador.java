import java.io.DataOutputStream;
import java.io.PrintWriter;


public class Jugador {
	int skin,posX,posY,vidas;
	String nombre;
	DataOutputStream escritor;
	float puntos;
	
	public Jugador(DataOutputStream escritor){
		this.escritor = escritor;
	}

	public int getSkin() {
		return skin;
	}

	public void setSkin(int skin) {
		this.skin = skin;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getVidas() {
		return vidas;
	}

	public void setVidas(int vidas) {
		this.vidas = vidas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getPuntos() {
		return puntos;
	}

	public void setPuntos(float puntos) {
		this.puntos = puntos;
	}
	
	public String toStirng(){
		return getNombre()+" "+getPosX()+"-"+getPosY();
	}
}
